require('dotenv').config()

module.exports = {
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: {
    // title: 'Basic',
    titleTemplate: t => (t ? t + ' | ' : '') + 'Crime Predictor',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1' },
      { hid: 'description', name: 'description', content: 'Capacitamos a tu equipo de ventas B2B para el uso estratégico de LinkedIn con la finalidad de aumentar tus leads y posicionar tu marca con los decisores de compra.' },
      { hid: 'og:description', name: 'og:description', content: 'Capacitamos a tu equipo de ventas B2B para el uso estratégico de LinkedIn con la finalidad de aumentar tus leads y posicionar tu marca con los decisores de compra.' }
    ],
    link: [
      { rel: 'icon', type: 'image/png', href: '/images/service-chatbot-green.png' }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#FFFFFF' },

  /*
  ** Global CSS
  */
  css: [
    '~/assets/css/app.css'
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/dotenv',

    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/axios'
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
