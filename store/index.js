export const state = () => ({
  results: [],
  chartdata: [],
  crimes: [
    {id: 1, name: 'Delitos aduaneros'},
    {id: 2, name: 'Delitos ambientales'},
    {id: 3, name: 'Delitos contra el estado y la defensa nacional'},
    {id: 4, name: 'Delitos contra el honor'},
    {id: 5, name: 'Delitos contra el orden financiero y monetario'},
    {id: 6, name: 'Delitos contra el patrimonio'},
    {id: 7, name: 'Delitos contra la administración pública'},
    {id: 8, name: 'Delitos contra la confianza y la buena fe en los negocios'},
    {id: 9, name: 'Delitos contra la familia'},
    {id: 10, name: 'Delitos contra la fe pública'},
    {id: 11, name: 'Delitos contra la humanidad'},
    {id: 12, name: 'Delitos contra la libertad'},
    {id: 13, name: 'Delitos contra la seguridad pública'},
    {id: 14, name: 'Delitos contra la tranquilidad pública'},
    {id: 15, name: 'Delitos contra la vida el cuerpo y la salud'},
    {id: 17, name: 'Delitos contra los poderes del estado y el orden constitucional'},
    {id: 18, name: 'Delitos tributarios'},
    {id: 19, name: 'Lavado de activos'},
    {id: 21, name: 'Delitos contra el orden económico'}
  ],
  content: [
    {
      field: 'Género',
      name: 'genero',
      type: 'select',
      options: [
        {label: 'Hombre', value: '1'},
        {label: 'Mujer', value: '2'}
      ]
    },
    {
      field: 'Estado Civil',
      name: 'e_civil',
      type: 'select',
      options: [
        {label: 'Conviviente', value: '1'},
        {label: 'Casado(a)', value: '2'},
        {label: 'Viudo(a)', value: '3'},
        {label: 'Divorciado(a)', value: '4'},
        {label: 'Separado(a)', value: '5'},
        {label: 'Soltero(a)', value: '6'}
      ]
    },
    {
      field: 'Religión',
      name: 'religion',
      type: 'select',
      options: [
        {label: 'Católica', value: '1'},
        {label: 'Evangélica', value: '2'},
        {label: 'Mormón', value: '3'},
        {label: 'Adventista', value: '4'},
        {label: 'Testigo de Jehová', value: '5'},
        {label: 'Otra', value: '6'},
        {label: 'Ninguna', value: '7'}
      ]
    },
    {
      field: 'Edad',
      name: 'edad',
      type: 'input'
    },
    {
      field: 'Nivel de estudios',
      name: 'p104_1',
      type: 'select',
      options: [
        {label: 'Sin nivel', value: '1'},
        {label: 'Educación inicial', value: '2'},
        {label: 'Primaria incompleta', value: '3'},
        {label: 'Primaria completa', value: '4'},
        {label: 'Secundaria incompleta', value: '5'},
        {label: 'Secundaria completa', value: '6'},
        {label: 'Superior no universitaria incompleta', value: '7'},
        {label: 'Superior no universitaria completa', value: '8'},
        {label: 'Superior universitaria incompleta', value: '9'},
        {label: 'Superior universitaria completa', value: '10'},
        {label: 'Postgrado', value: '11'}
      ]
    },
    {
      field: 'Durante sus estudios, ¿usted se relacionó con compañeros(as) que tuvieron problemas con la ley?',
      name: 'p106',
      type: 'select',
      options: [
        {label: 'Sí', value: '1'},
        {label: 'No', value: '2'},
        {label: 'No sabe / No contesta', value: '0'}
      ]
    },
    {
      field: '¿Consume drogas?',
      name: 'p109_1',
      type: 'select',
      options: [
        {label: 'Sí', value: '1'},
        {label: 'No', value: '2'},
        {label: 'No contesta', value: '3'}
      ]
    },
    {
      field: '¿Consume bebidas alcohólicas?',
      name: 'p109_2',
      type: 'select',
      options: [
        {label: 'Sí', value: '1'},
        {label: 'No', value: '2'},
        {label: 'No contesta', value: '3'}
      ]
    },
    {
      field: '¿Consume cigarrillos?',
      name: 'p109_3',
      type: 'select',
      options: [
        {label: 'Sí', value: '1'},
        {label: 'No', value: '2'},
        {label: 'No contesta', value: '3'}
      ]
    },
    {
      field: '¿Ha trabajado alguna vez?',
      name: 'p114',
      type: 'select',
      options: [
        {label: 'Sí', value: '1'},
        {label: 'No', value: '2'}
      ]
    },
    {
      field: 'Usted se desempeñó en su ocupación o negocio como:',
      name: 'p117',
      type: 'select',
      options: [
        {label: 'Empleador o patrono', value: '1'},
        {label: 'Trabajador dependiente', value: '2'},
        {label: 'Empleado', value: '3'},
        {label: 'Obrero', value: '4'},
        {label: 'Trabajador familiar no remunerado', value: '5'},
        {label: 'Trabajador del hogar', value: '6'},
        {label: 'Otro', value: '7'},
        {label: 'Valor de omisión', value: '0'}
      ]
    },
    {
      field: '¿Cuál es la razón principal por la que usted no ha trabajado?',
      name: 'p118',
      type: 'select',
      options: [
        {label: 'Falta de estudios', value: '1'},
        {label: 'Problemas de salud', value: '2'},
        {label: 'Por tener antecedentes penales/Judiciales', value: '3'},
        {label: 'Responsabilidades familiares', value: '4'},
        {label: 'Estaba estudiando', value: '5'},
        {label: 'No necesitaba trabajar', value: '6'},
        {label: 'Valor de omisión', value: '0'}
      ]
    },
    {
      field: '¿Hasta qué edad vivió con su mamá?',
      name: 'p122',
      type: 'input'
    },
    {
      field: '¿Hasta qué edad vivió con su papá?',
      name: 'p124',
      type: 'input'
    },
    {
      field: 'Cuando usted era niño (de 5 a 12 años). ¿algunos de sus padres o las personas que asumieron ese rol le pegaban?',
      name: 'p126',
      type: 'select',
      options: [
        {label: 'Sí, siempre', value: '1'},
        {label: 'Sí, a veces', value: '2'},
        {label: 'No', value: '3'},
        {label: 'No contesta', value: '4'}
      ]
    },
    {
      field: 'Cuándo usted era niño/a (de 5 a 12 años), ¿sus padres o los adultos que vivían con usted tomaban alcohol/licor frecuentemente?',
      name: 'p127',
      type: 'select',
      options: [
        {label: 'Sí', value: '1'},
        {label: 'No', value: '2'},
        {label: 'No sabe / No contesta', value: '3'}
      ]
    },
    {
      field: 'Cuándo usted era niño/a (de 5 a 12 años), ¿sus padres o los adultos que vivían con usted consumían droga?',
      name: 'p128',
      type: 'select',
      options: [
        {label: 'Sí', value: '1'},
        {label: 'No', value: '2'},
        {label: 'No sabe / No contesta', value: '3'}
      ]
    },
    {
      field: '¿A tu mamá le pegaba tu papá o su pareja?',
      name: 'p129',
      type: 'select',
      options: [
        {label: 'Sí', value: '1'},
        {label: 'No', value: '2'},
        {label: 'No aplica', value: '3'},
        {label: 'No sabe / No contesta', value: '4'}
      ]
    },
    {
      field: '¿Algún miembro de su familia estuvo preso en un establecimiento penitenciario alguna vez?',
      name: 'p133',
      type: 'select',
      options: [
        {label: 'Sí', value: '1'},
        {label: 'No', value: '2'},
        {label: 'No recuerda / No contesta', value: '3'}
      ]
    },
    {
      field: 'Antes de cumplir los 18 años de edad. ¿algun(os) de su(s) mejor(es) amigo(s) cometía(n) delitos?',
      name: 'p135',
      type: 'select',
      options: [
        {label: 'Sí', value: '1'},
        {label: 'No', value: '2'},
        {label: 'No recuerda / No contesta', value: '3'}
      ]
    },
    {
      field: 'En el barrio donde vivía antes de cumplir los 18 años de edad, ¿habían pandillas o bandas delictivas?',
      name: 'p136',
      type: 'select',
      options: [
        {label: 'Sí', value: '1'},
        {label: 'No', value: '2'},
        {label: 'No recuerda / No contesta', value: '3'}
      ]
    },
    {
      field: '¿Usted tiene hijos?',
      name: 'p137',
      type: 'select',
      options: [
        {label: 'Sí', value: '1'},
        {label: 'No', value: '2'}
      ]
    },
    {
      field: '¿Usted se ha sentido discriminado(a) en algún lugar alguna vez?',
      name: 'p139',
      type: 'select',
      options: [
        {label: 'Sí', value: '1'},
        {label: 'No', value: '2'}
      ]
    }
  ]
})

export const mutations = {
  SET_RESULTS (state, data) {
    state.results = data
    state.chartdata = [
      ['Crimen', 'Porcentaje de tendencia'],
      ['Delitos aduaneros', matchCrime(data, 1)],
      ['Delitos ambientales', matchCrime(data, 2)],
      ['Delitos contra el estado y la defensa nacional', matchCrime(data, 3)],
      ['Delitos contra el honor', matchCrime(data, 4)],
      ['Delitos contra el orden financiero y monetario', matchCrime(data, 5)],
      ['Delitos contra el patrimonio', matchCrime(data, 6)],
      ['Delitos contra la administración pública', matchCrime(data, 7)],
      ['Delitos contra la confianza y la buena fe en los negocios', matchCrime(data, 8)],
      ['Delitos contra la familia', matchCrime(data, 9)],
      ['Delitos contra la fe pública', matchCrime(data, 10)],
      ['Delitos contra la humanidad', matchCrime(data, 11)],
      ['Delitos contra la libertad', matchCrime(data, 12)],
      ['Delitos contra la seguridad pública', matchCrime(data, 13)],
      ['Delitos contra la tranquilidad pública', matchCrime(data, 14)],
      ['Delitos contra la vida el cuerpo y la salud', matchCrime(data, 15)],
      ['Delitos contra los poderes del estado y el orden constitucional', matchCrime(data, 17)],
      ['Delitos tributarios', matchCrime(data, 18)],
      ['Lavado de activos', matchCrime(data, 19)],
      ['Delitos contra el orden económico', matchCrime(data, 21)]
    ]
  }
}

function matchCrime (data, id) {
  var temp
  for (var i = 0; i < data.length; i++) {
    if (data[i].crimeId === id) {
      temp = data[i].output * 100
      return temp
    }
  }
}
