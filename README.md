# basic

> My gnarly Nuxt.js project

## Build Setup

``` bash
# install dependencies
$ npm install

# create a .env file and set this 
API_URL=https://crime-predictor-api.herokuapp.com/crimePredictor

# serve with hot reload at localhost:3800
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).
